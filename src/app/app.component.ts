import { Component } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  apiUrl = "https://api.zonky.cz/loans/marketplace";
  crossOriginApi = "https://crossorigin.me/";
  ratingList: string[] = ["A", "AA", "AAA", "AAAA", "AAAAA"];
  currentRating: string = null;
  averageLoanValue: number = null;

  constructor(private httpClient: HttpClient) {}

  /**
   * Select currently choosed rating and store to currentRating variable
   */
  chooseRating(rating) {
    if (rating && this.ratingList.includes(rating)) {
      this.currentRating = rating;
      try {
        this.getLoans();
      } catch (error) {
        alert(error);
      }
    }
  }

  /**
   * get list of loand from API by seleted rating
   */
  getLoans() {
    if (this.currentRating) {
      /*
       * Beacause crossorigin.me service was not working (it was returning error 522 even for google.com page)
       * I added example json file with downloaded API content. It should work just like API request but in offline mode...
       * If service starts to work, please replace const url by commented part
       */
      //let params = "?rating__eq=" + this.currentRating;
      //const url = this.crossOriginApi + this.apiUrl + params;
      const url = "/assets/" + this.currentRating + ".json";
      this.httpClient.get(url).subscribe(data => {
        this.calculateAverage(data);
      });
    } else {
      throw new Error("Rating is not seleted");
    }
  }

  /**
   * From REST data calculates average value
   */
  calculateAverage(data) {
    let sum = 0,
      count = 0;
    data.forEach(item => {
      if (!isNaN(item.amount)) {
        sum += parseFloat(item.amount);
      }
      count++;
    });
    this.averageLoanValue = sum / count;
    console.log(this.averageLoanValue);
  }
}
